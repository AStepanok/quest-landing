var express = require('express'),
    app  = express(),
    request = require('request'),
    bodyParser = require('body-parser');

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended: true}));

app.use('/public', express.static('public'));

app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));
app.use('/tether', express.static(__dirname + '/node_modules/tether/dist/'));
app.use('/slick', express.static(__dirname + '/node_modules/slick/slick/'));
app.use('/odometer', express.static(__dirname + '/node_modules/odometer/'));


app.get('/', function (req, res) {


    request('http://izolate55.ru/api/56634', function (error, response, body) {

        body = JSON.parse(body);
        var times = ['12:20','14:10','16:00','17:50','19:40','21:30','23:20'];
        var dates = [];

        var ruDays = ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'];

        var temp = body.filter(x => x.time == "14:10");

        for (var i=0; i< temp.length;i++) {
            var items = temp[i].date.split('-');
            dates.push({
                dateStr: temp[i].date,
                dayIndex: (new Date(parseInt(items[0]),parseInt(items[1])-1,parseInt(items[2]))).getDay(),
                dayNum: parseInt(items[2])
            });

        }

        res.render('index', {
            times: times,
            dates: dates,
            data: body,
            ruDays: ruDays
        });
    });


});

app.post('/', function (req, res) {
    var data = {
        time: req.body.time,
        date: req.body.date,
        email: req.body.email,
        phone: req.body.tel,
        first_name: req.body.name.split(' ')[0],
        price: req.body.price,
        players: req.body.num,



        comment: '_',
        family_name: req.body.name.split(' ')[1],
        source: 'source'




    };
    console.log(data);
    request({
        url: 'http://izolate55.ru/structure/bookapi/56634',
        headers: {'content-type' : 'application/x-www-form-urlencoded'},
        method: 'POST',
        form: data
    }, function(error, response, body){
        console.log(response.body);
        if (response.body.success == true) {
            console.log('true');
            res.end('{"success" : "Игра забронирована", "status" : 200}');
        } else {
            res.end('{"error" : '+response.body.message+', "status" : 500}');
        }
    });
        

});


app.listen(8081, function () {
    console.log("Server is running...");
});