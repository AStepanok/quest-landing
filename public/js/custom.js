var max = 0;
var onceCollapsed = false;
var clicked=true;

var pricePerPerson;
var bDay;
var bMonth;
var bDate;
var bTime;
var bTotalPrice;
var numOfPlayersж

var btn;

$(document).ready(function () {
    //    $('.slick-gal').slick({
    //        
    //    });


    $('.slider-for').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        focusOnSelect: true,
        centerMode: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: false,

                    slidesToShow: 1
                }
            }
        ]
    });

    $('.bookingSlick').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        infinite: false,
        prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-circle-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-circle-right"></i></button>',

    });

    $('.testimonialsSlider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-circle-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-circle-right"></i></button>',
    });

    var numbers = $('.odometer');

    for (var i = 0; i< numbers.length; i++) {

        if (checkVisible(numbers[i])) {

            $(numbers[i]).text($(numbers[i]).attr('data-number'));
        }
    }

});

$('.card-header button').on('click', function (e) {
    clicked=true;
    if ($(this).parents('.card').children('.container').children('.collapse').hasClass('show')) {
        clicked=false;
        e.stopPropagation();
    }


});

function checkVisible(elm) {
    var rect = elm.getBoundingClientRect();
    var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
    return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
}

$(window).on('scroll', function () {

    var item = document.getElementsByClassName('contacts')[0];




    if (checkVisible(item) && !onceCollapsed) {
        console.log('yeah');
        $('.card:first-of-type .collapse').collapse('show');
        onceCollapsed = true;
    }

});



// Select all links with hashes
$('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function (event) {
        var me = $(this); // On-page links
        if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
            location.hostname == this.hostname
        ) {


            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();



                $('html, body').animate({
                    scrollTop: (target.offset().top-73)
                }, 1000, function () {
                    var link = $(me).attr('href');
                    if (link == '#sales' ||
                        link == '#age' ||
                        link == '#needed' ||
                        link == '#cert') {

                        switch (link) {
                            case '#cert':
                                $('#cert .collapse').collapse('show');
                                onceCollapsed = true;
                                break;
                            case '#sales':
                                $('#sales .collapse').collapse('show');
                                onceCollapsed = true;
                                break;
                            case '#needed':
                                $('#needed .collapse').collapse('show');
                                onceCollapsed = true;
                                break;
                            case '#age':
                                $('#age .collapse').collapse('show');
                                onceCollapsed = true;
                                break;
                            default:
                                console.log($(this).attr('href'));
                                break;
                        }


                    }


                    return true;
                });
            }
        }
    });

$('.card .collapse').on('shown.bs.collapse', function () {
    if (!clicked) {
        // Figure out element to scroll to
        var target = $(this).parents('.card');

        // target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        // Does a scroll target exist?

        // Only prevent default if animation is actually gonna happen
        event.preventDefault();


        $('html, body').animate({
            scrollTop: (target.offset().top - 73)
        }, 1000, function () {
            return true;
        });
    } else {
        clicked=false;
    }
});

$('.bookGameBtn').on('click', function () {
    bDay = $(this).attr('data-day');
    bMonth = $(this).attr('data-month');
    bTime = $(this).attr('data-time');
    pricePerPerson = $(this).attr('data-pperson');
    bDate = $(this).attr('data-date');


   $('#bookModal').modal('show');

   btn=$(this);

   updateBookInfo();
});

$('select').on('change', function () {
    updateBookInfo();
});

function updateBookInfo() {
    $('.bookDateAndTime').text(bDay + '.' + bMonth + ', ' + bTime);
    bTotalPrice = pricePerPerson*$('select').val();
    $('.price').text(bTotalPrice+'₽');
    $('#price').val(bTotalPrice);
    console.log($('#price').val());
}

$('#bookingForm').submit(function() {
    var name = $('#name').val();
    var tel = $('#tel').val();
    var email = $('#email').val();
    var num = $('#numOfPlayers').val();


    $.post({
        url: '/',
        data: {name: name, tel: tel, email: email, num: num, date: bDate, time: bTime, price: bTotalPrice},
        success: function (res) {
            $('#bookModal').modal('hide');
            $('#successModal').modal('show');
            btn.addClass('disabled');
            btn.removeClass('bookGameBtn');
            btn.prop('disabled',true);

        },
        error: function (res) {
            $('#bookModal').modal('hide');
            $('#errorModal').modal('show');

        }
    });
    return false;
});

$(window).on('scroll', function () {

    var numbers = $('.odometer');

    for (var i = 0; i< numbers.length; i++) {

        if (checkVisible(numbers[i])) {

            $(numbers[i]).text($(numbers[i]).attr('data-number'));
        }
    }
});
